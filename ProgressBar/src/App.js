import React from 'react'
import { useEffect, useState } from 'react'
import ProgressBar from "@darfys/react-simple-progressbar"
import './App.css'

const App = () => {

  const [position, setPosition] = useState(0);

  console.log(ProgressBar)

  useEffect(() => {
    window.addEventListener("scroll", onScroll)

    return () => window.removeEventListener("scroll", onScroll)
  }, [])

  const onScroll = () => {
    console.log(window.scrollY / ((document.body.scrollHeight - window.innerHeight) / 100))
    setPosition(window.scrollY / ((document.body.scrollHeight - window.innerHeight) / 100))
  }

  return (
    <div className="App">
      <ProgressBar value={position} />
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo dolor ad molestiae tempore cumque delectus possimus provident error ratione perferendis, natus quisquam quasi repudiandae, nemo, a neque quos quibusdam molestias!</p>
    </div>
  );
}

export default App;
