const styles = {
  progressBar: {
    height: 6,
    display: "flex",
    alignItems: "center",
    backgroundColor: "#d9d9d9",
    position: "fixed",
    zIndex: 10,
    width: "100%",
    top: 0,
    left: 0,
  },
  progressIndicator: {
    backgroundColor: "#2972f0",
    height: "100%",
  }
}

const ProgressBar = ({value}) => {
  return (
    <div style={styles.progressBar}>
      <div style={{...styles.progressIndicator, width: `${value}%`}}></div>
    </div>
  )
}

export default ProgressBar